{ 
	"targets": [ 
		{
			"target_name": "mailAutomation",  
			"sources": [ "sources/main.cpp",
			             "sources/domains.cpp",
			             "sources/hMailServer.cpp",
			             "sources/domain.cpp",
			             "sources/accounts.cpp",
			             "sources/account.cpp",
			             "sources/imapFolders.cpp",
			             "sources/imapFolder.cpp",
			             "sources/imapMessages.cpp",
			             "sources/imapMessage.cpp"
            ]
		} 
	] 
}