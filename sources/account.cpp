#include "stdafx.h"
#include "hMailServer.h"
#include "account.h"
#include "imapFolders.h"

namespace mailServer {

	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::PropertyCallbackInfo;
	using v8::ObjectTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;

	Account::Account(IDispatch* account) {
        //std::cout << "account creating" << std::endl;
        this->account = account;
        this->imapFolders = new IMAPFolders(account);
	}

	Account::~Account() {
	    //std::cout << "account destroying" << std::endl;
	    //release();
	}

    int Account::release() {
        //std::cout << "account releasing" << std::endl;
        imapFolders->release();
        if (account == NULL) return HMAILSERVER_NOT_INITIALIZED;
        HRESULT hr = account->Release();
        account = NULL;
        if (FAILED(hr)) {
            std::cout << "account releasing failed" << std::endl;
            return HMAILSERVER_RELEASE_FAILED;
        }
        return 0;
    }

    void Account::Release(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        Account* account = ObjectWrap::Unwrap<Account>(args.Holder());

        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be a function()"));
            return;
        }

    	int result = account->release();
    	if (result != 0) {
    	    args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_NOT_INITIALIZED, L"Account is not initialized"));
            return;
    	}

    	args.GetReturnValue().Set(v8::Null(isolate));
    	return;
    }

    Local<Object> Account::CreateObject(Isolate* isolate, Account* account) {
        Local<ObjectTemplate> templ = ObjectTemplate::New(isolate);
        templ->SetInternalFieldCount(1);
        NODE_SET_METHOD(templ, "release", Release);
        NODE_SET_METHOD(templ, "save", Save);
        NODE_SET_METHOD(templ, "delete", Delete);
	    Local<Object> obj = templ->NewInstance();
        obj->Set(String::NewFromUtf8(isolate, "folders"), IMAPFolders::CreateObject(isolate, account->imapFolders));
        account->Wrap(obj);
        return obj;
    }

    bool Account::setAddress(BSTR address) {
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = address;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, account, L"Address", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool Account::setPassword(BSTR password) {
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = password;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, account, L"Password", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool Account::setActive(bool active) {
	    VARIANT parm[1];
	    parm[0].vt = VT_I4;
	    if (active)
	        parm[0].lVal = -1;
	    else
	        parm[0].lVal = 0;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, account, L"Active", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool Account::setMaxSize(int maxSize) {
	    VARIANT parm[1];
	    parm[0].vt = VT_I4;
	    parm[0].lVal = maxSize;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, account, L"MaxSize", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool Account::save() {
	    VARIANT result;
	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, &result, account, L"Save", 0, NULL);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool Account::del() {
	    VARIANT x;
	    VariantInit(&x);
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &x, account, L"ID", 0, NULL);
	    if (FAILED(hr)) {
	        return false;
	    }
	    VARIANT result;
	    VARIANT parm[1];
	    parm[0].vt = VT_I4;
	    parm[0].lVal = x.lVal;
	    hr = Server::AutoWrap(DISPATCH_METHOD, &result, account, L"DeleteByDBID", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    int Account::getId() {
	    VARIANT result;
	    VariantInit(&result);
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, account, L"ID", 0, NULL);
	    if (FAILED(hr)) return 0;
	    return result.lVal;
    }

    void Account::Save(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function()"));
            return;
        }

        Account* account = ObjectWrap::Unwrap<Account>(args.Holder());
        v8::Handle<Object> object = args.This();

        args.GetReturnValue().Set(v8::Null(isolate));

        BSTR address = SysAllocString((LPCWSTR)* v8::String::Value(object->Get(String::NewFromUtf8(isolate, "address"))));
        bool res = account->setAddress(address);
        SysFreeString(address);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set account address"));
            return;
        }

        BSTR password = SysAllocString((LPCWSTR)* v8::String::Value(object->Get(String::NewFromUtf8(isolate, "password"))));
        res = account->setPassword(password);
        SysFreeString(password);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set account password"));
            return;
        }

        bool active = object->Get(String::NewFromUtf8(isolate, "active"))->BooleanValue();
        res = account->setActive(active);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set account active"));
            return;
        }

        int maxSize = object->Get(String::NewFromUtf8(isolate, "maxSize"))->Int32Value();
        res = account->setMaxSize(maxSize);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set account maxSize"));
            return;
        }

        res = account->save();
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t save account"));
            return;
        }

        int id = account->getId();
        args.Holder()->Set(String::NewFromUtf8(isolate, "id"), Number::New(isolate, id));
    }

    void Account::Delete(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function()"));
            return;
        }

        Account* account = ObjectWrap::Unwrap<Account>(args.Holder());
        v8::Handle<Object> object = args.This();

        args.GetReturnValue().Set(v8::Null(isolate));
        bool res = account->del();
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t delete account"));
            return;
        }
    }
}