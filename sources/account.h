#pragma once
#include "stdafx.h"
#include "imapFolders.h"
#include <node.h>
#include <node_object_wrap.h>

namespace mailServer {
     class Account : public node::ObjectWrap {
                public:
                    static v8::Local<v8::Object> CreateObject(v8::Isolate* isolate, Account* accounts);
                    Account(IDispatch* domain);
                    ~Account();
                    int release();
                private:
                    IDispatch* account;
                    IMAPFolders* imapFolders;
                    bool setAddress(BSTR address);
                    bool setPassword(BSTR password);
                    bool setActive(bool active);
                    bool setMaxSize(int maxSize);
                    int Account::getId();
                    bool save();
                    bool del();
                    static void Save(const v8::FunctionCallbackInfo<v8::Value>& args);
                    static void Delete(const v8::FunctionCallbackInfo<v8::Value>& args);
                    v8::Local<v8::String> address;
                    v8::Local<v8::String> password;
                    v8::Local<v8::Integer> maxSize;
                    v8::Local<v8::Boolean> active;
                    static void Release(const v8::FunctionCallbackInfo<v8::Value>& args);
         };
}