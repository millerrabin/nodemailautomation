#include "stdafx.h"
#include "domain.h"
#include "hMailServer.h"
#include "accounts.h"
#include "account.h"

namespace mailServer {

	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::ObjectTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;

	Accounts::Accounts(IDispatch* domain) {
        this->accounts = Request(domain);
	}

    Accounts::~Accounts() {
	    //std::cout << "accounts destroying" << std::endl;
	    //accounts->Release();
    }

    int Accounts::release() {
        //std::cout << "accounts releasing" << std::endl;
        if (accounts == NULL) return HMAILSERVER_NOT_INITIALIZED;
        HRESULT hr = accounts->Release();
        accounts = NULL;
        if (FAILED(hr)) {
            std::cout << "accounts releasing failed" << std::endl;
            return HMAILSERVER_RELEASE_FAILED;
        }
        return 0;
    }

	IDispatch* Accounts::Request(IDispatch* domain) {
        //std::cout << "accounts requesting" << std::endl;
        VARIANT dResult;
        VariantInit(&dResult);
        HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &dResult, domain, L"Accounts", 0, NULL);
        if (FAILED(hr)) return NULL;
        return dResult.pdispVal;
    }

    Local<Object> Accounts::CreateObject(Isolate* isolate, Accounts* accounts) {
        Local<ObjectTemplate> templ = ObjectTemplate::New(isolate);
        NODE_SET_METHOD(templ, "add", Add);
        NODE_SET_METHOD(templ, "byAddress", ByAddress);
        NODE_SET_METHOD(templ, "delete", DeleteById);

        templ->SetInternalFieldCount(1);
	    Local<Object> obj = templ->NewInstance();
        accounts->Wrap(obj);
        return obj;
    }

    void Accounts::Add(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        Accounts* accounts = ObjectWrap::Unwrap<Accounts>(args.Holder());

        if ((args.Length() != 1) || !args[0]->IsFunction()) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(callback)"));
            return;
        }

        Local<Value> err = v8::Null(isolate);
        VARIANT result;
        VariantInit(&result);
 	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, &result, accounts->accounts, L"Add", 0, NULL);
 	    if (FAILED(hr))
 	        err = Server::CreateErrorObject(isolate, HMAILSERVER_INSTANCE_FAILED, L"Add account failed");

        args.GetReturnValue().Set(err);
        Local<Function> cb = Local<Function>::Cast(args[0]);
        Local<Value> obj = v8::Null(isolate);
        Account* acc = NULL;
        if (err->IsNull()) {
            acc = new Account(result.pdispVal);
            obj = Account::CreateObject(isolate, acc);
        }

        const unsigned argc = 2;
        Local<Value> argv[argc] = {  err, obj };
        cb->Call(Null(isolate), argc, argv);

        if (acc != NULL) acc->release();
    }

    void Accounts::ByAddress(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        Accounts* accounts = ObjectWrap::Unwrap<Accounts>(args.Holder());

        if ((args.Length() != 2) || !args[0]->IsString() || !args[1]->IsFunction()) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(string, callback)"));
            return;
        }

        Local<Value> err = v8::Null(isolate);
        VARIANT result;
        BSTR dstr = SysAllocString((LPCWSTR)* v8::String::Value(args[0]->ToString()));
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = dstr;

 	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, accounts->accounts, L"ItemByAddress", 1, parm);
        SysFreeString(dstr);

 	    if (FAILED(hr))
 	        err = Server::CreateErrorObject(isolate, HMAILSERVER_INSTANCE_FAILED, L"Get account failed");

        args.GetReturnValue().Set(err);
        Local<Function> cb = Local<Function>::Cast(args[1]);
        Local<Value> obj = v8::Null(isolate);
        Account* acc = NULL;
        if (err->IsNull()) {
            acc = new Account(result.pdispVal);
            obj = Account::CreateObject(isolate, acc);
        }

        const unsigned argc = 2;
        Local<Value> argv[argc] = {  err, obj };
        cb->Call(Null(isolate), argc, argv);
        if (acc != NULL) {
            acc->release();
        }
    }

    void Accounts::DeleteById(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        Accounts* accounts = ObjectWrap::Unwrap<Accounts>(args.Holder());

        if ((args.Length() != 1) || !args[0]->IsUint32()) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(integer)"));
            return;
        }

        Local<Value> err = v8::Null(isolate);
        VARIANT result;
        VariantInit(&result);

	    int val = args[0]->ToUint32()->Value();

	    VARIANT parm[1];
	    parm[0].vt = VT_I4;
	    parm[0].lVal = val;
 	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, &result, accounts->accounts, L"DeleteByDBID", 1, parm);
 	    if (FAILED(hr))
            err = Server::CreateErrorObject(isolate, HMAILSERVER_INSTANCE_FAILED, L"Delete by id account failed");
        args.GetReturnValue().Set(err);
    }
}