#pragma once
#include "stdafx.h"
#include <node.h>
#include <node_object_wrap.h>

namespace mailServer {
     class Domain;
     class Accounts : public node::ObjectWrap {
                public:
                    static v8::Local<v8::Object> CreateObject(v8::Isolate* isolate, Accounts* accounts);
                    Accounts(IDispatch* accounts);
                    ~Accounts();
                    int release();
                private:
                    static IDispatch* Request(IDispatch* domain);
                    IDispatch* accounts;
                    static void Add(const v8::FunctionCallbackInfo<v8::Value>& args);
                    static void ByAddress(const v8::FunctionCallbackInfo<v8::Value>& args);
                    static void DeleteById(const v8::FunctionCallbackInfo<v8::Value>& args);
     };
}