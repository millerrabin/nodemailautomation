#include "stdafx.h"
#include "domain.h"

#include "hMailServer.h"
#include "domains.h"
#include "accounts.h"

namespace mailServer {

	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::ObjectTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;

	Domain::Domain(IDispatch* domain) {
        //std::cout << "domain creating" << std::endl;
        this->domain = domain;
        this->accounts = new Accounts(domain);
	}

	Domain::~Domain() {
	    //std::cout << "domain destroying" << std::endl;
	    //release();
	}

    Local<Object> Domain::CreateObject(Isolate* isolate, Domain* domain) {
        Local<ObjectTemplate> templ = ObjectTemplate::New(isolate);
        NODE_SET_METHOD(templ, "release", Release);
        templ->SetInternalFieldCount(1);
	    Local<Object> obj = templ->NewInstance();
	    Local<Object> acc = Accounts::CreateObject(isolate, domain->accounts);
        obj->Set(String::NewFromUtf8(isolate, "accounts"), acc);
        domain->Wrap(obj);
        return obj;
    }

    int Domain::release() {
        //std::cout << "domain releasing" << std::endl;
        if (domain == NULL) return HMAILSERVER_NOT_INITIALIZED;
        accounts->release();
        HRESULT hr = domain->Release();
        domain = NULL;
        if (FAILED(hr)) return HMAILSERVER_RELEASE_FAILED;
        return 0;
    }

    void Domain::Release(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        Domain* domain = ObjectWrap::Unwrap<Domain>(args.Holder());

        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be a function()"));
            return;
        }

    	int result = domain->release();
    	if (result != 0) {
    	    args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_NOT_INITIALIZED, L"Domain not initialized"));
            return;
    	}

    	args.GetReturnValue().Set(v8::Null(isolate));
    	return;
    }
}