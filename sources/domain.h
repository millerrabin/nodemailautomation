#pragma once
#include "stdafx.h"
#include "accounts.h"
#include <node.h>
#include <node_object_wrap.h>

namespace mailServer {
     class Domain : public node::ObjectWrap {
                public:
                    static v8::Local<v8::Object> CreateObject(v8::Isolate* isolate, Domain* domain);
                    Domain(IDispatch* domain);
                    ~Domain();
                    int release();
                private:
                    Accounts* accounts;
                    IDispatch* domain;
                    static void Release(const v8::FunctionCallbackInfo<v8::Value>& args);
         };
}