#include "stdafx.h"
#include "domain.h"

#include "hMailServer.h"
#include "domains.h"

namespace mailServer {

	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::ObjectTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;

	Domains::Domains(IDispatch* server) {
        domains = Request(server);
	}

	IDispatch* Domains::Request(IDispatch* server) {
        //std::cout << "domains requesting" << std::endl;
        VARIANT dResult;
        VariantInit(&dResult);
        HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &dResult, server, L"Domains", 0, NULL);
        if (FAILED(hr)) return NULL;
        return dResult.pdispVal;
    }

	Domains::~Domains() {
	    //std::cout << "domains destroying" << std::endl;
	    //release();
	}

	int Domains::release() {
	    //std::cout << "domains releasing" << std::endl;
	    if (domains == NULL) {
	        std::cout << "domains not initialized" << std::endl;
	        return HMAILSERVER_NOT_INITIALIZED;
	    }
	    HRESULT hr = domains->Release();
	    domains = NULL;
        if (FAILED(hr)) return HMAILSERVER_RELEASE_FAILED;
        return 0;
	}

    Local<Object> Domains::CreateObject(Isolate* isolate, Domains* domains) {
        Local<ObjectTemplate> templ = ObjectTemplate::New(isolate);
        NODE_SET_METHOD(templ, "get", Get);
        templ->SetInternalFieldCount(1);
	    Local<Object> obj = templ->NewInstance();
        domains->Wrap(obj);
        return obj;
    }

    void Domains::Get(const FunctionCallbackInfo<Value>& args) {
		Isolate* isolate = args.GetIsolate();
		Domains* domains = ObjectWrap::Unwrap<Domains>(args.Holder());

        if ((args.Length() != 2) || !args[0]->IsString() ||  !args[1]->IsFunction()) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(string domainName, function callback)"));
            return;
        }

        Local<Value> err = v8::Null(isolate);
        BSTR dstr = SysAllocString((LPCWSTR)* v8::String::Value(args[0]->ToString()));
	    VARIANT result;
	    VariantInit(&result);
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = dstr;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, domains->domains, L"ItemByName", 1, parm);
	    SysFreeString(dstr);

	    if (FAILED(hr))
	        err = Server::CreateErrorObject(isolate, HMAILSERVER_INSTANCE_FAILED, L"domain is not found");
        args.GetReturnValue().Set(err);

        Local<Function> cb = Local<Function>::Cast(args[1]);
        Local<Value> obj = v8::Null(isolate);

        Domain* dm = NULL;
        if (err->IsNull()) {
            dm = new Domain(result.pdispVal);
            obj = Domain::CreateObject(isolate, dm);
        }

        const unsigned argc = 2;
        Local<Value> argv[argc] = {  err, obj };
        cb->Call(Null(isolate), argc, argv);
        if (dm != NULL) dm->release();
		return;
    }
}