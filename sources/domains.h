#pragma once
#include "hMailServer.h"
#include "stdafx.h"
#include <node.h>
#include <node_object_wrap.h>

namespace mailServer {
     class Domains : public node::ObjectWrap {
                public:
                    static v8::Local<v8::Object> CreateObject(v8::Isolate* isolate, Domains* domains);
                    Domains(IDispatch* server);
                    ~Domains();
                    int release();
                private:
                    static IDispatch* Request(IDispatch* server);
                    IDispatch* domains;
                    static void Get(const v8::FunctionCallbackInfo<v8::Value>& args);
         };
}
