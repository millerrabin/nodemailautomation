#include "hMailServer.h"
#include "domains.h"
#include "stdafx.h"

namespace mailServer {

	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::FunctionTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;

	Persistent<Function> Server::constructor;

	Server::Server(IDispatch* server) {
	    this->hMailServer = server;
	    this->domains = NULL;
	}
	Server::~Server() {
        //std::cout << "hMailServer destroying" << std::endl;
        //this->release();
	}

	int Server::release() {
	    //std::cout << "server releasing" << std::endl;
	    if (domains != NULL) domains->release();
	    if (hMailServer == NULL) return HMAILSERVER_NOT_INITIALIZED;
	    HRESULT hr = hMailServer->Release();
        hMailServer = NULL;
        if (FAILED(hr)) return HMAILSERVER_RELEASE_FAILED;
        return 0;
	}

    HRESULT Server::AutoWrap(int autoType, VARIANT *pvResult, IDispatch *pDisp, LPOLESTR ptName, int count, VARIANT* args) {
	    if (pDisp == NULL) {
	        //std::cout << "pDisp is null" << std::endl;
	        return -1;
	    }

        DISPPARAMS dp = { NULL, NULL, 0, 0 };
        dp.cArgs = count;
        dp.rgvarg = args;

    	DISPID dispidNamed = DISPID_PROPERTYPUT;
    	DISPID dispID;
    	HRESULT hr = pDisp->GetIDsOfNames(IID_NULL, &ptName, 1, LOCALE_USER_DEFAULT, &dispID);
    	if (FAILED(hr)) {
    	    //std::cout << "IDsOfNames failed" << std::endl;
    	    return -1;
    	}

        if (autoType & DISPATCH_PROPERTYPUT) {
            dp.cNamedArgs = 1;
            dp.rgdispidNamedArgs = &dispidNamed;
        }

        hr = pDisp->Invoke(dispID, IID_NULL, LOCALE_SYSTEM_DEFAULT, autoType, &dp, pvResult, NULL, NULL);
        if (FAILED(hr)) {
            //std::cout << "invoke failed" << std::endl;
            return -1;
        }
	    return 0;
    }

    int Server::authenticate(IDispatch* server, LPCWSTR login, LPCWSTR password) {
        VARIANT parm[2];
        parm[1].vt = VT_BSTR;
	    parm[1].bstrVal = SysAllocString(login);
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = SysAllocString(password);

    	VARIANT result;
	    HRESULT hr = AutoWrap(DISPATCH_METHOD, &result, server, L"Authenticate", 2, parm);

	    SysFreeString(parm[0].bstrVal);
	    SysFreeString(parm[1].bstrVal);

	    if (FAILED(hr))
	        return HMAILSERVER_INVOKE_ERROR;

	    if (result.boolVal == 0)
	        return HMAILSERVER_AUTHENTICATION_FAILED;
	    return 0;
    }

    void Server::Authenticate(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        Server* server = ObjectWrap::Unwrap<Server>(args.Holder());

        if ((args.Length() != 2) || !args[0]->IsString() || !args[1]->IsString()) {
            args.GetReturnValue().Set(CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(string login, string password)"));
            return;
        }

        if (server->hMailServer == NULL) {
            args.GetReturnValue().Set(CreateErrorObject(isolate, HMAILSERVER_NOT_INITIALIZED, L"Mail server not initialized"));
            return;
        }

	    int result = server->authenticate(server->hMailServer,
	                                      (LPCWSTR)* v8::String::Value(args[0]->ToString()),
	                                      (LPCWSTR)* v8::String::Value(args[1]->ToString()));

        if (result != 0) {
            args.GetReturnValue().Set(CreateErrorObject(isolate, result, L"Authentication failed"));
            return;
        }

        args.GetReturnValue().Set(v8::Null(isolate));
        server->domains = new Domains(server->hMailServer);
        args.Holder()->Set(String::NewFromUtf8(isolate, "domains"), Domains::CreateObject(isolate, server->domains));
	    return;
    }

    void Server::Request(const FunctionCallbackInfo<Value>& args) {
		//std::cout << "server requesting" << std::endl;
		Isolate* isolate = args.GetIsolate();

        Local<Value> err = v8::Null(isolate);
        if ((args.Length() != 1) || !args[0]->IsFunction()) {
            args.GetReturnValue().Set(CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(callback)"));
            return;
        }

	    IDispatch* hMailServer = NULL;
	    CLSID clsid;
	    HRESULT hr = CLSIDFromProgID(L"hMailServer.Application", &clsid);
	    if (!FAILED(hr)) {
            hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IDispatch, (void **)&hMailServer);
            	if (FAILED(hr))
            	    err = CreateErrorObject(isolate, HMAILSERVER_INSTANCE_FAILED, L"hMailServer instance failed");
	    } else
	        err = CreateErrorObject(isolate, HMAILSERVER_CLSID_FAILED, L"hMailServer com object is not found");

        args.GetReturnValue().Set(err);

        Local<Function> cb = Local<Function>::Cast(args[0]);
        Local<Value> obj = v8::Null(isolate);

        Server* srv = NULL;
        if (err->IsNull()) {
            srv = new Server(hMailServer);
            obj = CreateRequestObject(isolate, srv);
        }

        const unsigned argc = 2;
        Local<Value> argv[argc] = {  err, obj };
        cb->Call(Null(isolate), argc, argv);
        if (srv != NULL) srv->release();
		return;
    }

    void Server::Release(const FunctionCallbackInfo<Value>& args) {
		Isolate* isolate = args.GetIsolate();
		Server* server = ObjectWrap::Unwrap<Server>(args.Holder());

        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be a function()"));
            return;
        }

	    int result = server->release();
	    if (result != 0) {
	        args.GetReturnValue().Set(CreateErrorObject(isolate, HMAILSERVER_NOT_INITIALIZED, L"Mail server not initialized"));
            return;
	    }

	    args.GetReturnValue().Set(v8::Null(isolate));
	    return;
    }

	void Server::Init(Local<Object> exports) {
		NODE_SET_METHOD(exports, "request", Request);
	}

    Local<Object> Server::CreateRequestObject(Isolate* isolate, Server* server) {
        Local<v8::ObjectTemplate> templ = v8::ObjectTemplate::New(isolate);
        NODE_SET_METHOD(templ, "authenticate", Authenticate);
        NODE_SET_METHOD(templ, "release", Release);
        NODE_SET_METHOD(templ, "newMessage", NewMessage);
        templ->SetInternalFieldCount(1);
	    Local<Object> obj = templ->NewInstance();
        server->Wrap(obj);
        return obj;
    }

    Local<Object> Server::CreateErrorObject(Isolate* isolate, int code, LPCWSTR message) {
        Local<Object> obj = Object::New(isolate);
        obj->Set(String::NewFromUtf8(isolate, "code"), Number::New(isolate, code));
        obj->Set(String::NewFromUtf8(isolate, "message"), String::NewFromTwoByte(isolate, (uint16_t*)message));
        return obj;
    }

    void Server::NewMessage(const FunctionCallbackInfo<Value>& args) {
		Isolate* isolate = args.GetIsolate();
        Local<Value> err = v8::Null(isolate);
        Local<Value> obj = v8::Null(isolate);
        if ((args.Length() != 1) || !args[0]->IsFunction()) {
            args.GetReturnValue().Set(CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(callback)"));
            return;
        }

        IMAPMessage* message = IMAPMessage::Request();
        if (message == NULL) {
            err = CreateErrorObject(isolate, HMAILSERVER_INSTANCE_FAILED, L"Can`t initialise hMailServer message");
        }  else {
            obj = IMAPMessage::CreateObject(isolate, message);
        }

        Local<Function> cb = Local<Function>::Cast(args[0]);
        const unsigned argc = 2;
        Local<Value> argv[argc] = {  err, obj };
        cb->Call(Null(isolate), argc, argv);
        args.GetReturnValue().Set(v8::Null(isolate));
        message->release();
		return;
   }

}