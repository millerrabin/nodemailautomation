#pragma once
#include "stdafx.h"
#include "domains.h"
#include "imapMessage.h"
#include <node.h>
#include <node_object_wrap.h>

namespace mailServer {

    const int HMAILSERVER_CLSID_FAILED = -1;
    const int HMAILSERVER_INSTANCE_FAILED = -2;
    const int HMAILSERVER_RELEASE_FAILED = -3;
    const int HMAILSERVER_INVOKE_ERROR = -4;
    const int HMAILSERVER_NOT_INITIALIZED = -5;
    const int HMAILSERVER_DOMAINS_FAILED = -6;
    const int HMAILSERVER_DOMAIN_NOTFOUND = -7;
    const int HMAILSERVER_INVALID_PARAMETERS = -7;
    const int HMAILSERVER_AUTHENTICATION_FAILED = -8;

	class Server : public node::ObjectWrap {
	public:
		static void Init(v8::Local<v8::Object> exports);
		static v8::Local<v8::Object> CreateErrorObject(v8::Isolate* isolate, int code, LPCWSTR message);
		static HRESULT AutoWrap(int autoType, VARIANT *pvResult, IDispatch *pDisp, LPOLESTR ptName, int count, VARIANT* args);
		int release();
	private:
		Server(IDispatch* server);
		~Server();
        static void Request(const v8::FunctionCallbackInfo<v8::Value>& args);
        static void Release(const v8::FunctionCallbackInfo<v8::Value>& args);
		static void New(const v8::FunctionCallbackInfo<v8::Value>& args);
        static void Authenticate(const v8::FunctionCallbackInfo<v8::Value>& args);
		Domains* domains;
		static v8::Persistent<v8::Function> constructor;
		static v8::Local<v8::Object> CreateRequestObject(v8::Isolate* isolate, Server* server);
		int authenticate(IDispatch* server, LPCWSTR login, LPCWSTR password);
		static void NewMessage(const FunctionCallbackInfo<Value>& args);
		IDispatch* hMailServer;
	};
}
