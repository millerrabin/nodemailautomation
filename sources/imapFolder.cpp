#include "stdafx.h"
#include "hMailServer.h"
#include "imapFolder.h"

namespace mailServer {

	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::PropertyCallbackInfo;
	using v8::ObjectTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;

	IMAPFolder::IMAPFolder(IDispatch* folder) {
        //std::cout << "IMAPFolder created" << std::endl;
        this->folder = folder;
        this->messages = new IMAPMessages(folder);
	}

	IMAPFolder::~IMAPFolder() {
	    //std::cout << "IMAPFolder destroying" << std::endl;
	    //release();
	}

    int IMAPFolder::release() {
        //std::cout << "IMAPFolder releasing" << std::endl;
        messages->release();
        if (folder == NULL) return HMAILSERVER_NOT_INITIALIZED;
        folder->Release();
        folder = NULL;
        return 0;
    }

    void IMAPFolder::Release(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        IMAPFolder* folder = ObjectWrap::Unwrap<IMAPFolder>(args.Holder());

        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be a function()"));
            return;
        }

    	int result = folder->release();
    	if (result != 0) {
    	    args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_NOT_INITIALIZED, L"IMAPFolder is not initialized"));
            return;
    	}

    	args.GetReturnValue().Set(v8::Null(isolate));
    	return;
    }

    int IMAPFolder::getId() {
	    VARIANT result;
	    VariantInit(&result);
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, folder, L"ID", 0, NULL);
	    if (FAILED(hr)) return 0;
	    return result.lVal;
    }

    bool IMAPFolder::save() {
	    VARIANT result;
	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, &result, folder, L"Save", 0, NULL);
	    if (FAILED(hr)) return false;
	    return true;
    }

    Local<Object> IMAPFolder::CreateObject(Isolate* isolate, IMAPFolder* folder) {
        Local<ObjectTemplate> templ = ObjectTemplate::New(isolate);
        templ->SetInternalFieldCount(1);
        NODE_SET_METHOD(templ, "save", Save);
        NODE_SET_METHOD(templ, "release", Release);
	    Local<Object> obj = templ->NewInstance();
        obj->Set(String::NewFromUtf8(isolate, "messages"), IMAPMessages::CreateObject(isolate, folder->messages));
        obj->Set(String::NewFromUtf8(isolate, "id"), v8::Integer::New(isolate, folder->getId()));
        folder->Wrap(obj);
        return obj;
    }

    bool IMAPFolder::setName(BSTR name) {
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = name;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, folder, L"Name", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    void IMAPFolder::Save(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function()"));
            return;
        }

        IMAPFolder* folder = ObjectWrap::Unwrap<IMAPFolder>(args.Holder());
        v8::Handle<Object> object = args.This();

        args.GetReturnValue().Set(v8::Null(isolate));

        BSTR name = SysAllocString((LPCWSTR)* v8::String::Value(object->Get(String::NewFromUtf8(isolate, "name"))));
        bool res = folder->setName(name);
        SysFreeString(name);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set name folder"));
            return;
        }

        res = folder->save();
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t save folder"));
            return;
        }

        int id = folder->getId();
        args.Holder()->Set(String::NewFromUtf8(isolate, "id"), Number::New(isolate, id));
    }

}