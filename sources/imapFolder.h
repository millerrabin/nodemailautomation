#pragma once
#include "stdafx.h"
#include "imapMessages.h"
#include <node.h>
#include <node_object_wrap.h>

namespace mailServer {
    class IMAPFolder : public node::ObjectWrap {
                public:
                    static v8::Local<v8::Object> CreateObject(v8::Isolate* isolate, IMAPFolder* folder);
                    IMAPFolder(IDispatch* folder);
                    ~IMAPFolder();
                    int release();
                private:
                    IDispatch* folder;
                    IMAPMessages* messages;
                    static void IMAPFolder::Release(const v8::FunctionCallbackInfo<v8::Value>& args);
                    bool setName(BSTR name);
                    static void Save(const v8::FunctionCallbackInfo<v8::Value>& args);
                    bool save();
                    int getId();
    };
}