#include "stdafx.h"
#include "hMailServer.h"
#include "imapFolders.h"
#include "imapFolder.h"

namespace mailServer {

	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::PropertyCallbackInfo;
	using v8::ObjectTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;

	IMAPFolders::IMAPFolders(IDispatch* account) {
        this->folders = Request(account);
	}

	IMAPFolders::~IMAPFolders() {
	    //std::cout << "IMAPFolders destroying" << std::endl;
	    //release();
	}

	IDispatch* IMAPFolders::Request(IDispatch* account) {
        //std::cout << "IMAPFolders requesting" << std::endl;
        VARIANT dResult;
        VariantInit(&dResult);
        HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &dResult, account, L"IMAPFolders", 0, NULL);
        if (FAILED(hr)) return NULL;
        return dResult.pdispVal;
    }

    int IMAPFolders::release() {
        //std::cout << "IMAPFolders releasing" << std::endl;
        if (folders == NULL) return HMAILSERVER_NOT_INITIALIZED;
        HRESULT hr = folders->Release();
        folders = NULL;
        if (FAILED(hr)) return HMAILSERVER_RELEASE_FAILED;
        return 0;
    }

    IDispatch* IMAPFolders::add(BSTR name) {
	    VARIANT result;
        VariantInit(&result);
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = name;
	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, &result, folders, L"Add", 1, parm);
	    if (FAILED(hr)) return NULL;
	    return result.pdispVal;
    }

    Local<Object> IMAPFolders::CreateObject(Isolate* isolate, IMAPFolders* folders) {
        Local<ObjectTemplate> templ = ObjectTemplate::New(isolate);
        templ->SetInternalFieldCount(1);
        NODE_SET_METHOD(templ, "byName", ByName);
        NODE_SET_METHOD(templ, "add", Add);
        NODE_SET_METHOD(templ, "deleteById", DeleteById);
	    Local<Object> obj = templ->NewInstance();
        folders->Wrap(obj);
        return obj;
    }

    void IMAPFolders::DeleteById(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        IMAPFolders* folders = ObjectWrap::Unwrap<IMAPFolders>(args.Holder());

        if ((args.Length() != 1) || !args[0]->IsUint32()) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(integer)"));
            return;
        }

        Local<Value> err = v8::Null(isolate);
        VARIANT result;
        VariantInit(&result);

	    int val = args[0]->ToUint32()->Value();

	    VARIANT parm[1];
	    parm[0].vt = VT_I4;
	    parm[0].lVal = val;
 	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, &result, folders->folders, L"DeleteByDBID", 1, parm);
 	    if (FAILED(hr))
            err = Server::CreateErrorObject(isolate, HMAILSERVER_INSTANCE_FAILED, L"delete folder by id failed");
        args.GetReturnValue().Set(err);
    }

    void IMAPFolders::ByName(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        IMAPFolders* folders = ObjectWrap::Unwrap<IMAPFolders>(args.Holder());

        if ((args.Length() != 2) || !args[0]->IsString() || !args[1]->IsFunction()) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(string, callback)"));
            return;
        }

        Local<Value> err = v8::Null(isolate);
        VARIANT result;
        BSTR name = SysAllocString((LPCWSTR)* v8::String::Value(args[0]->ToString()));
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = name;

 	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, folders->folders, L"ItemByName", 1, parm);
        SysFreeString(name);

 	    if (FAILED(hr))
 	        err = Server::CreateErrorObject(isolate, HMAILSERVER_INSTANCE_FAILED, L"Get folder failed");

        args.GetReturnValue().Set(err);
        Local<Function> cb = Local<Function>::Cast(args[1]);
        Local<Value> obj = v8::Null(isolate);
        IMAPFolder* folder = NULL;
        if (err->IsNull()) {
            folder = new IMAPFolder(result.pdispVal);
            obj = IMAPFolder::CreateObject(isolate, folder);
        }

        const unsigned argc = 2;
        Local<Value> argv[argc] = {  err, obj };
        cb->Call(Null(isolate), argc, argv);
        if (folder != NULL) {
            folder->release();
        }
    }

    void IMAPFolders::Add(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        IMAPFolders* folders = ObjectWrap::Unwrap<IMAPFolders>(args.Holder());

        if ((args.Length() != 2) || !args[0]->IsString() || !args[1]->IsFunction()) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(name, callback)"));
            return;
        }

        Local<Value> err = v8::Null(isolate);
        BSTR name = SysAllocString((LPCWSTR)* v8::String::Value(args[0]->ToString()));
        IDispatch* ifld = folders->add(name);
        SysFreeString(name);

        if (ifld == NULL) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INSTANCE_FAILED, L"can`t add folder"));
            return;
        }

        Local<Function> cb = Local<Function>::Cast(args[1]);
        Local<Value> obj = v8::Null(isolate);
        IMAPFolder* fld = NULL;
        if (err->IsNull()) {
            fld = new IMAPFolder(ifld);
            obj = IMAPFolder::CreateObject(isolate, fld);
        }

        const unsigned argc = 2;
        Local<Value> argv[argc] = {  err, obj };
        cb->Call(Null(isolate), argc, argv);

        args.GetReturnValue().Set(err);
        if (fld != NULL) fld->release();
    }
}