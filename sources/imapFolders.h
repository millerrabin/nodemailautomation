#pragma once
#include "stdafx.h"
#include <node.h>
#include <node_object_wrap.h>

namespace mailServer {
    class IMAPFolders : public node::ObjectWrap {
                public:
                    static v8::Local<v8::Object> CreateObject(v8::Isolate* isolate, IMAPFolders* folders);
                    IMAPFolders(IDispatch* account);
                    ~IMAPFolders();
                    int release();
                private:
                    IDispatch* folders;
                    static void ByName(const FunctionCallbackInfo<Value>& args);
                    static void Add(const v8::FunctionCallbackInfo<v8::Value>& args);
                    static void DeleteById(const v8::FunctionCallbackInfo<v8::Value>& args);
                    IDispatch* add(BSTR name);
                    static IDispatch* Request(IDispatch* account);
         };
}