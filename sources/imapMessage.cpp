#include "stdafx.h"
#include "hMailServer.h"
#include "IMAPMessage.h"

namespace mailServer {

	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::PropertyCallbackInfo;
	using v8::ObjectTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;
	using v8::Handle;
	using v8::Array;

	IMAPMessage::IMAPMessage(IDispatch* message) {
        //std::cout << "IMAPMessage created" << std::endl;
        this->message = message;
	}

	IMAPMessage::~IMAPMessage() {
	    //std::cout << "IMAPMessage destroying" << std::endl;
	    //release();
	}

    int IMAPMessage::release() {
        //std::cout << "IMAPMessage releasing" << std::endl;
        if (message == NULL) return HMAILSERVER_NOT_INITIALIZED;
        message->Release();
        message = NULL;
        return 0;
    }

	IMAPMessage* IMAPMessage::Request() {
	    CLSID clsid;
	    HRESULT hr = CLSIDFromProgID(L"hMailServer.Message", &clsid);
	    if (FAILED(hr)) return NULL;
        IDispatch* message;
        hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IDispatch, (void **)&message);
        if (FAILED(hr)) return NULL;
        return new IMAPMessage(message);
	}

    void IMAPMessage::Release(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        IMAPMessage* message = ObjectWrap::Unwrap<IMAPMessage>(args.Holder());

        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be a function()"));
            return;
        }

    	int result = message->release();
    	if (result != 0) {
    	    args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_NOT_INITIALIZED, L"IMAPMessage is not initialized"));
            return;
    	}

    	args.GetReturnValue().Set(v8::Null(isolate));
    	return;
    }

    Local<Object> IMAPMessage::CreateObject(Isolate* isolate, IMAPMessage* message) {
        Local<ObjectTemplate> templ = ObjectTemplate::New(isolate);
        templ->SetInternalFieldCount(1);
        NODE_SET_METHOD(templ, "release", Release);
	    NODE_SET_METHOD(templ, "save", Save);
	    NODE_SET_METHOD(templ, "copy", Copy);
	    NODE_SET_METHOD(templ, "addRecipient", AddRecipient);
	    Local<Object> obj = templ->NewInstance();
        obj->Set(String::NewFromUtf8(isolate, "body"), String::NewFromTwoByte(isolate, message->getBody()));
        obj->Set(String::NewFromUtf8(isolate, "htmlBody"), String::NewFromTwoByte(isolate, message->getHTMLBody()));
        obj->Set(String::NewFromUtf8(isolate, "from"), String::NewFromTwoByte(isolate, message->getFrom()));
        obj->Set(String::NewFromUtf8(isolate, "fromAddress"), String::NewFromTwoByte(isolate, message->getFromAddress()));
        obj->Set(String::NewFromUtf8(isolate, "subject"), String::NewFromTwoByte(isolate, message->getSubject()));
        obj->Set(String::NewFromUtf8(isolate, "date"), String::NewFromTwoByte(isolate, message->getDate()));
        obj->Set(String::NewFromUtf8(isolate, "id"), v8::Integer::New(isolate, message->getId()));
        message->Wrap(obj);
        return obj;
    }

    uint16_t* IMAPMessage::getBody() {
	    VARIANT result;
	    VariantInit(&result);
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, message, L"Body", 0, NULL);
	    if (FAILED(hr)) return NULL;
	    return (uint16_t*) result.bstrVal;
    }

    uint16_t* IMAPMessage::getHTMLBody() {
    	VARIANT result;
    	VariantInit(&result);
    	HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, message, L"HTMLBody", 0, NULL);
    	if (FAILED(hr)) {
    	    std::cout << "HTML BODY FAILED" << std::endl;
    	    return NULL;
    	}
    	return (uint16_t*) result.bstrVal;
    }

    uint16_t* IMAPMessage::getFrom() {
        VARIANT result;
        VariantInit(&result);
        HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, message, L"From", 0, NULL);
        if (FAILED(hr)) {
            return NULL;
        }
        return (uint16_t*) result.bstrVal;
    }

    uint16_t* IMAPMessage::getFromAddress() {
        VARIANT result;
        VariantInit(&result);
        HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, message, L"FromAddress", 0, NULL);
        if (FAILED(hr)) {
            return NULL;
        }
        return (uint16_t*) result.bstrVal;
    }

    uint16_t* IMAPMessage::getSubject() {
        VARIANT result;
        VariantInit(&result);
        HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, message, L"Subject", 0, NULL);
        if (FAILED(hr)) {
            return NULL;
        }
        return (uint16_t*) result.bstrVal;
    }

    uint16_t* IMAPMessage::getDate() {
        VARIANT result;
        VariantInit(&result);
        HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, message, L"Date", 0, NULL);
        if (FAILED(hr)) {
            return NULL;
        }
        return (uint16_t*) result.bstrVal;
    }

     bool IMAPMessage::refresh() {
    	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, NULL, message, L"RefreshContent", 0, NULL);
    	    if (FAILED(hr)) return false;
    	    return true;
    }

    int IMAPMessage::getId() {
	    VARIANT result;
	    VariantInit(&result);
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, message, L"ID", 0, NULL);
	    if (FAILED(hr)) return 0;
	    return result.lVal;
    }

    bool IMAPMessage::setFrom(BSTR from) {
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = from;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, message, L"From", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool IMAPMessage::setFromAddress(BSTR address) {
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = address;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, message, L"FromAddress", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool IMAPMessage::setSubject(BSTR subject) {
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = subject;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, message, L"Subject", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool IMAPMessage::setBody(BSTR body) {
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = body;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, message, L"Body", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool IMAPMessage::setHTMLBody(BSTR body) {
	    VARIANT parm[1];
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = body;
	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYPUT, NULL, message, L"HTMLBody", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool IMAPMessage::addRecipient(BSTR recipient, BSTR recipientAddress) {
	    VARIANT parm[2];
	    parm[1].vt = VT_BSTR;
	    parm[1].bstrVal = recipient;
	    parm[0].vt = VT_BSTR;
	    parm[0].bstrVal = recipientAddress;

	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, NULL, message, L"AddRecipient", 2, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool IMAPMessage::save() {
	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, NULL, message, L"Save", 0, NULL);
	    if (FAILED(hr)) return false;
	    return true;
    }

    bool IMAPMessage::copy(int folderId) {
	    VARIANT parm[1];
	    parm[0].vt = VT_I4;
	    parm[0].lVal = folderId;
	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, NULL, message, L"Copy", 1, parm);
	    if (FAILED(hr)) return false;
	    return true;
    }

    void IMAPMessage::Save(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function()"));
            return;
        }

        IMAPMessage* message = ObjectWrap::Unwrap<IMAPMessage>(args.Holder());
        v8::Handle<Object> object = args.This();

        BSTR from = SysAllocString((LPCWSTR)* v8::String::Value(object->Get(String::NewFromUtf8(isolate, "from"))));
        bool res = message->setFrom(from);
        SysFreeString(from);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set from field"));
            return;
        }

        BSTR fromAddress = SysAllocString((LPCWSTR)* v8::String::Value(object->Get(String::NewFromUtf8(isolate, "fromAddress"))));
        res = message->setFromAddress(fromAddress);
        SysFreeString(fromAddress);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set from address"));
            return;
        }

        BSTR subject = SysAllocString((LPCWSTR)* v8::String::Value(object->Get(String::NewFromUtf8(isolate, "subject"))));
        res = message->setSubject(subject);
        SysFreeString(subject);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set subject"));
            return;
        }

        BSTR body = SysAllocString((LPCWSTR)* v8::String::Value(object->Get(String::NewFromUtf8(isolate, "body"))));
        res = message->setBody(body);
        SysFreeString(body);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set body"));
            return;
        }

        BSTR htmlBody = SysAllocString((LPCWSTR)* v8::String::Value(object->Get(String::NewFromUtf8(isolate, "htmlBody"))));
        res = message->setHTMLBody(htmlBody);
        SysFreeString(htmlBody);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t set htmlBody"));
            return;
        }
        res = message->save();
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t save message"));
            return;
        }

        int id = message->getId();
        args.Holder()->Set(String::NewFromUtf8(isolate, "id"), Number::New(isolate, id));

        args.GetReturnValue().Set(v8::Null(isolate));
    }

    void IMAPMessage::Copy(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        if ((args.Length() != 1) || !args[0]->IsUint32()) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(integer)"));
            return;
        }

        IMAPMessage* message = ObjectWrap::Unwrap<IMAPMessage>(args.Holder());
        v8::Handle<Object> object = args.This();

        int folderId = args[0]->ToUint32()->Value();
        bool res = message->copy(folderId);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t copy message"));
            return;
        }
        args.GetReturnValue().Set(v8::Null(isolate));
    }

    void IMAPMessage::AddRecipient(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        if ((args.Length() != 2) || !args[0]->IsString() || !args[1]->IsString()) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(string, string)"));
            return;
        }

        IMAPMessage* message = ObjectWrap::Unwrap<IMAPMessage>(args.Holder());
        v8::Handle<Object> object = args.This();


        BSTR name = SysAllocString((LPCWSTR)* v8::String::Value(args[0]->ToString()));
        BSTR address = SysAllocString((LPCWSTR)* v8::String::Value(args[1]->ToString()));
        bool res = message->addRecipient(name, address);
        SysFreeString(name);
        SysFreeString(address);
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t add recipient"));
            return;
        }
        args.GetReturnValue().Set(v8::Null(isolate));
    }
}