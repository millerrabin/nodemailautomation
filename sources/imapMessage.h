#pragma once
#include "stdafx.h"
#include <node.h>
#include <node_object_wrap.h>

namespace mailServer {
	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::FunctionTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;

    class IMAPMessage : public node::ObjectWrap {
                public:
                    static v8::Local<v8::Object> CreateObject(v8::Isolate* isolate, IMAPMessage* message);
                    IMAPMessage(IDispatch* message);
                    ~IMAPMessage();
                    int release();
                    int getId();
                    static IMAPMessage* Request();
                private:
                    IDispatch* message;
                    static void IMAPMessage::Release(const v8::FunctionCallbackInfo<v8::Value>& args);
                    uint16_t* getBody();
                    uint16_t* getHTMLBody();
                    uint16_t* getFrom();
                    uint16_t* getFromAddress();
                    uint16_t* getSubject();
                    uint16_t* getDate();
                    bool refresh();
                    static void Save(const FunctionCallbackInfo<Value>& args);
                    static void Copy(const FunctionCallbackInfo<Value>& args);
                    static void AddRecipient(const FunctionCallbackInfo<Value>& args);
                    bool addRecipient(BSTR recipient, BSTR recipientAddress);
                    bool setBody(BSTR body);
                    bool setHTMLBody(BSTR body);
                    bool setSubject(BSTR subject);
                    bool setFromAddress(BSTR address);
                    bool setFrom(BSTR from);
                    bool save();
                    bool copy(int folderId);
    };
}