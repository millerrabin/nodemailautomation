#include "stdafx.h"
#include "hMailServer.h"
#include "imapMessages.h"
#include "imapMessage.h"

namespace mailServer {

	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::PropertyCallbackInfo;
	using v8::ObjectTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Number;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;

	IMAPMessages::IMAPMessages(IDispatch* folder) {
        this->messages = Request(folder);
	}

	IMAPMessages::~IMAPMessages() {
	    //std::cout << "IMAPMessages destroying" << std::endl;
	    //release();
	}

    int IMAPMessages::release() {
        //std::cout << "IMAPMessages releasing" << std::endl;
        if (messages == NULL) return HMAILSERVER_NOT_INITIALIZED;
        HRESULT hr = messages->Release();
        messages = NULL;
        if (FAILED(hr)) {
            std::cout << "IMAPMessages release failed" << std::endl;
            return HMAILSERVER_RELEASE_FAILED;
        }
        return 0;
    }

    Local<Object> IMAPMessages::CreateObject(Isolate* isolate, IMAPMessages* messages) {
        Local<ObjectTemplate> templ = ObjectTemplate::New(isolate);
        templ->SetInternalFieldCount(1);
        NODE_SET_METHOD(templ, "item", Item);
	    NODE_SET_METHOD(templ, "byId", ById);
        NODE_SET_METHOD(templ, "add", Add);
	    NODE_SET_METHOD(templ, "deleteById", Delete);
	    NODE_SET_METHOD(templ, "clear", Clear);
	    Local<Object> obj = templ->NewInstance();
        obj->Set(String::NewFromUtf8(isolate, "count"), v8::Integer::New(isolate, messages->getCount()));
        messages->Wrap(obj);
        return obj;
    }

	IDispatch* IMAPMessages::Request(IDispatch* folder) {
        //std::cout << "IMAPMessages requesting" << std::endl;
        VARIANT dResult;
        VariantInit(&dResult);
        HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &dResult, folder, L"Messages", 0, NULL);
        if (FAILED(hr)) return NULL;
        return dResult.pdispVal;
    }

    IMAPMessage* IMAPMessages::item(int index) {
	    VARIANT result;
	    VariantInit(&result);
        VARIANT parm[1];
        parm[0].vt = VT_I4;
        parm[0].lVal = index;

	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, messages, L"Item", 1, parm);
	    if (FAILED(hr)) {
	        std::cout << "Autowrap failed" << std::endl;
	        return NULL;
	    }
	    if (result.pdispVal == NULL) return NULL;
	    IMAPMessage* message = new IMAPMessage(result.pdispVal);
	    return message;
    }

    IMAPMessage* IMAPMessages::add() {
	    VARIANT result;
	    VariantInit(&result);
	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, &result, messages, L"Add", 0, NULL);
	    if (FAILED(hr)) return NULL;
	    if (result.pdispVal == NULL) return NULL;
	    IMAPMessage* message = new IMAPMessage(result.pdispVal);
	    return message;
    }

    IMAPMessage* IMAPMessages::byId(int id) {
	    VARIANT result;
	    VariantInit(&result);
        VARIANT parm[1];
        parm[0].vt = VT_I4;
        parm[0].lVal = id;

	    HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, messages, L"ItemByDBID", 1, parm);
	    if (FAILED(hr)) {
	        std::cout << "Autowrap failed" << std::endl;
	        return NULL;
	    }
	    if (result.pdispVal == NULL) return NULL;
	    IMAPMessage* message = new IMAPMessage(result.pdispVal);
	    return message;
    }

    bool IMAPMessages::deleteById(int id) {
        VARIANT parm[1];
        parm[0].vt = VT_I4;
        parm[0].lVal = id;
	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, NULL, messages, L"DeleteByDBID", 1, parm);
	    if (FAILED(hr)) {
	        return false;
	    }
	    return true;
    }

    bool IMAPMessages::clear() {
	    VARIANT result;
	    HRESULT hr = Server::AutoWrap(DISPATCH_METHOD, &result, messages, L"Clear", 0, NULL);
	    if (FAILED(hr)) return false;
	    return true;
    }

    void IMAPMessages::Item(const FunctionCallbackInfo<Value>& args) {
            Isolate* isolate = args.GetIsolate();

            if ((args.Length() != 1) || !args[0]->IsUint32()) {
                args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(integer)"));
                return;
            }

            IMAPMessages* messages = ObjectWrap::Unwrap<IMAPMessages>(args.Holder());
            v8::Handle<Object> object = args.This();
            int val = args[0]->ToUint32()->Value();
            IMAPMessage* message = messages->item(val);
            if (message == NULL) {
                args.GetReturnValue().Set(v8::Null(isolate));
                return;
            }
            args.GetReturnValue().Set(IMAPMessage::CreateObject(isolate, message));
     }

    void IMAPMessages::Add(const FunctionCallbackInfo<Value>& args) {
            Isolate* isolate = args.GetIsolate();

            if ((args.Length() != 0)) {
                args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function()"));
                return;
            }

            IMAPMessages* messages = ObjectWrap::Unwrap<IMAPMessages>(args.Holder());
            v8::Handle<Object> object = args.This();
            IMAPMessage* message = messages->add();
            if (message == NULL) {
                args.GetReturnValue().Set(v8::Null(isolate));
                return;
            }
            args.GetReturnValue().Set(IMAPMessage::CreateObject(isolate, message));
     }

     void IMAPMessages::ById(const FunctionCallbackInfo<Value>& args) {
            Isolate* isolate = args.GetIsolate();

            if ((args.Length() != 1) || !args[0]->IsUint32()) {
                args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(integer)"));
                return;
            }

            IMAPMessages* messages = ObjectWrap::Unwrap<IMAPMessages>(args.Holder());
            v8::Handle<Object> object = args.This();
            int val = args[0]->ToUint32()->Value();
            IMAPMessage* message = messages->byId(val);

            if (message == NULL) {
                args.GetReturnValue().Set(v8::Null(isolate));
                return;
            }

            args.GetReturnValue().Set(IMAPMessage::CreateObject(isolate, message));
    }

     void IMAPMessages::Delete(const FunctionCallbackInfo<Value>& args) {
            Isolate* isolate = args.GetIsolate();

            if ((args.Length() != 1) || !args[0]->IsUint32()) {
                args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function(integer)"));
                return;
            }

            IMAPMessages* messages = ObjectWrap::Unwrap<IMAPMessages>(args.Holder());
            v8::Handle<Object> object = args.This();
            int val = args[0]->ToUint32()->Value();

            bool res = messages->deleteById(val);;
            if (!res) {
                args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"message is not Found"));
                return;
            }
            args.GetReturnValue().Set(v8::Null(isolate));
    }

    int IMAPMessages::getCount() {
        VARIANT result;
        VariantInit(&result);
        HRESULT hr = Server::AutoWrap(DISPATCH_PROPERTYGET, &result, messages, L"Count", 0, NULL);
        if (FAILED(hr)) {
            std::cout << "Message count Failed" << std::endl;
            return NULL;
        }
        return result.lVal;
    }

    void IMAPMessages::Clear(const FunctionCallbackInfo<Value>& args) {
        Isolate* isolate = args.GetIsolate();
        if ((args.Length() != 0)) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"must be function()"));
            return;
        }

        IMAPMessages* messages = ObjectWrap::Unwrap<IMAPMessages>(args.Holder());
        v8::Handle<Object> object = args.This();

        args.GetReturnValue().Set(v8::Null(isolate));

        bool res = messages->clear();
        if (!res) {
            args.GetReturnValue().Set(Server::CreateErrorObject(isolate, HMAILSERVER_INVALID_PARAMETERS, L"can`t clear folder"));
            return;
        }
    }
}