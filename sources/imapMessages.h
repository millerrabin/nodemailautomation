#pragma once
#include "stdafx.h"
#include "imapMessage.h"
#include <node.h>
#include <node_object_wrap.h>

namespace mailServer {
    class IMAPMessages : public node::ObjectWrap {
                public:
                    static v8::Local<v8::Object> CreateObject(v8::Isolate* isolate, IMAPMessages* messages);
                    IMAPMessages(IDispatch* messages);
                    ~IMAPMessages();
                    int release();
                private:
                    IDispatch* messages;
                    static void Item(const v8::FunctionCallbackInfo<v8::Value>& args);
                    static void ById(const v8::FunctionCallbackInfo<v8::Value>& args);
                    static void Add(const v8::FunctionCallbackInfo<v8::Value>& args);
                    static void Delete(const v8::FunctionCallbackInfo<v8::Value>& args);
                    static void Clear(const v8::FunctionCallbackInfo<v8::Value>& args);
                    IMAPMessage* item(int index);
                    IMAPMessage* add();
                    IMAPMessage* byId(int id);
                    bool deleteById(int id);
                    bool clear();
                    static IDispatch* Request(IDispatch* folder);
                    int getCount();
         };
}