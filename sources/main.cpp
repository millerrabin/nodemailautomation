#include <node.h>
#include "hMailServer.h"

namespace mailServer {

	using v8::Local;
	using v8::Object;
	using node::AtExit;

    void OnExit(void* arg) {
        CoUninitialize();
    }

	void InitAll(Local<Object> exports) {
		CoInitialize(NULL);
		Server::Init(exports);
		AtExit(OnExit);
	}

	NODE_MODULE(hMailServer, InitAll)

}
